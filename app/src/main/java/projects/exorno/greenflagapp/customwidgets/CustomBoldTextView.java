package projects.exorno.greenflagapp.customwidgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by marios on 2017/09/28.
 *
 * An extension Class of textview that incoperates custom font
 */
@SuppressLint("AppCompatCustomView")
public class CustomBoldTextView extends TextView {
    public CustomBoldTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        Typeface tfNorm = Typeface.createFromAsset(context.getAssets(), "fonts/MuseoSans_500.ttf");
        setTypeface(tfNorm);

    }
}
