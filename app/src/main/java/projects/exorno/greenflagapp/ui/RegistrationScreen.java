package projects.exorno.greenflagapp.ui;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.regex.Matcher;

import projects.exorno.greenflagapp.R;
import projects.exorno.greenflagapp.customwidgets.CustomTextView;
import projects.exorno.greenflagapp.tools.UserDatabase;

public class RegistrationScreen extends AppCompatActivity {

    private final String EMAIL_FIELD="email";
    private final String PASS_FIELD="password";
    private final String CONF_FIELD="confirmation";


    //UI elements declarations
    EditText emailEdit;
    EditText passEdit;
    EditText conEdit;
    LinearLayout emailErr;
    LinearLayout passErr;
    Button nxtButton;

    public CustomTextView emailErrText;
    public CustomTextView passErrText;
    UserDatabase database;

    Context context;
    public Validator validator;

    public boolean validEmail,validPass,validConf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_screen);

        database =  new UserDatabase(this);
        validator=  new Validator();
        initForm();
    }

    /**
     * Initialises the UI elements and adds watched to the text fields
     */
    private void initForm(){

        context = this;

        //initialising email field
        emailEdit = (EditText) findViewById(R.id.email_field);
        emailEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                validator.validateEmail(emailEdit.getText().toString().trim());
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        //initialising password field
        passEdit = (EditText) findViewById(R.id.password_field);
        passEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                validator.validatePassword(passEdit.getText().toString(),
                        conEdit.getText().toString());
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        //initialising confirmation field
        conEdit = (EditText) findViewById(R.id.confirm_field);
        conEdit.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                validator.validateConfirmation(passEdit.getText().toString(),
                        conEdit.getText().toString());
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        //initialising confirmation button
        nxtButton = (Button) findViewById(R.id.nxt_btn);
        nxtButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                database.insert(emailEdit.getText().toString().trim(),passEdit.getText().toString());
                nxtButton.setEnabled(false);
                Toast.makeText(RegistrationScreen.this, "Account Saved", Toast.LENGTH_LONG).show();

            }
        });

        //initialising the error elements
        emailErr = (LinearLayout)findViewById(R.id.email_error);
        passErr = (LinearLayout)findViewById(R.id.pass_error);
        emailErrText = (CustomTextView)findViewById(R.id.email_err_text);
        passErrText = (CustomTextView)findViewById(R.id.pass_err_text);

    }

    public void backButton(View view){
        onBackPressed();
    }

    /**
     * Updates the UI accordingly dependently on the validation results
     */
    private  void updateUI(String field, boolean status){

        switch (field){
            case EMAIL_FIELD:
                if(status){
                    emailEdit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                    emailEdit.setBackgroundResource(R.drawable.textview_green_border);
                    emailErr.setVisibility(View.GONE);
                    validEmail = true;
                    validator.validateForm();

                }
                else{
                    emailEdit.setCompoundDrawables(null, null, null, null);
                    emailEdit.setBackgroundResource(R.drawable.textview_red_border);
                    emailErr.setVisibility(View.VISIBLE);
                    validEmail = false;
                    validator.validateForm();
                }
                break;

            case PASS_FIELD:
                if(status){
                    passEdit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                    passEdit.setBackgroundResource(R.drawable.textview_green_border);
                    passErr.setVisibility(View.GONE);
                    validPass = true;
                    validator.validateForm();
                }
                else{
                    passEdit.setCompoundDrawables(null, null, null, null);
                    passEdit.setBackgroundResource(R.drawable.textview_red_border);
                    passErr.setVisibility(View.VISIBLE);
                    validPass = false;
                    validator.validateForm();
                }
                break;

            case CONF_FIELD:
                if(status){
                    conEdit.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.tick, 0);
                    conEdit.setBackgroundResource(R.drawable.textview_green_border);
                    passErr.setVisibility(View.GONE);
                    validConf = true;
                    validator.validateForm();
                }
                else{
                    conEdit.setCompoundDrawables(null, null, null, null);
                    conEdit.setBackgroundResource(R.drawable.textview_red_border);
                    passErr.setVisibility(View.VISIBLE);
                    validConf = false;
                    validator.validateForm();
                }
                break;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    /**
     * Inner class that carrys out all the validation action for the RegistrationScreen
     */
    public class Validator {

        public void validateEmail(String email) {

            //matcher checks for correct email format
            Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(email);
            UserDatabase database =  new UserDatabase(context);
            if(!matcher.matches() /*&& !email.isEmpty()*/) {
                emailErrText.setText(getString(R.string.email_error_no_valid));
                updateUI(EMAIL_FIELD, false);
            }
            else if(database.emailExists(email)) {
                emailErrText.setText(getString(R.string.email_error_already_exists));
                updateUI(EMAIL_FIELD, false);
            }
            else
                updateUI(EMAIL_FIELD, true);
        }

        public void validatePassword(String password, String confirmation) {

            if(!passwordCheck(password)){
                passErrText.setText(getString(R.string.pass_error_no_valid));
                updateUI(PASS_FIELD, false);
            }
            else if(!confirmation.isEmpty() && !password.matches(confirmation)) {
                passErrText.setText(getString(R.string.pass_error_no_match));
                updateUI(PASS_FIELD,false);
            }
            else
                updateUI(PASS_FIELD, true);
        }

        public void validateConfirmation(String password, String confirmation) {

            if(!passwordCheck(confirmation)){
                passErrText.setText(getString(R.string.pass_error_no_valid));
                updateUI(CONF_FIELD,false);
            }
            else if(!confirmation.isEmpty() && !password.matches(confirmation)) {
                passErrText.setText(getString(R.string.pass_error_no_match));
                updateUI(CONF_FIELD,false);
            }
            else
                updateUI(CONF_FIELD,true);
        }

        /**
         * Validates all the conditions of the form
         */
        public void validateForm(){
            nxtButton.setEnabled(validEmail && validPass && validConf);
        }

        /**
         * Checks if supplied password meets requirements using regex
         */
        private boolean passwordCheck(String toCheck){

            return (toCheck.length()>=8 && toCheck.matches(".*\\d+.*")
                    && toCheck.matches(".*[A-Z].*")
                    && toCheck.matches(".*[a-z].*"));
        }

    }
}
