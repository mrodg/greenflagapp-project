package projects.exorno.greenflagapp.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by marios on 2017/10/30.
 */

public class UserDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="acounts.db";
    private static final String TABLE_NAME="acounts";
    private static final String COLUMN_EMAIL="email";
    private static final String COLUMN_PASS="password";
    private SQLiteDatabase db;
    private static final String TABLE_CREATE = "create table acounts (id integer primary key autoincrement not null ," +
            "email text not null, password text not null)";

    public UserDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLE_CREATE);
        this.db=sqLiteDatabase;

    }
    /**
     * Inserts user details to the database.
     */
    public void insert(String theEmail,String pass){

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_EMAIL,theEmail);
        values.put(COLUMN_PASS,pass);
        db.insert(TABLE_NAME, null,values);
        db.close();
    }
    /**
     * Uses the supplied email to check of the user already exists in the database.
     */
    public boolean emailExists(String email){
        String query = "SELECT * FROM " + TABLE_NAME+" WHERE "+COLUMN_EMAIL+" =?";
        db = this.getReadableDatabase();

        Cursor c = db.rawQuery(query, new String[]{email});
        if(c.getCount()>0){
            c.close();
            return true;
        }else {
            c.close();
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String query = "DROP TABLE IF EXIST "+ TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }
}