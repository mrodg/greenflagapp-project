import android.support.v7.app.AppCompatActivity;
import android.view.View;

import org.junit.Test;

import projects.exorno.greenflagapp.ui.RegistrationScreen;
import projects.exorno.greenflagapp.ui.HomeScreen;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by marios on 2017/11/01.
 */

public class BasicAppUnitTests {

    @Test
    public void viewVisibilityCheck() throws Exception {

        assertThat(View.GONE, is(8));
        assertThat(View.VISIBLE, is(0));
    }

    @Test
    public void registrationParentCheck() {
        RegistrationScreen testRegScren = new RegistrationScreen();
        assertThat(testRegScren, instanceOf(AppCompatActivity.class));
    }

    @Test
    public void homeParentCheck() {
        HomeScreen testHomeScren = new HomeScreen();
        assertThat(testHomeScren, instanceOf(AppCompatActivity.class));
    }
}
