import android.support.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import projects.exorno.greenflagapp.ui.RegistrationScreen;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by marios on 2017/11/01.
 */

public class ValidationTests {

    @Rule
    public ActivityTestRule<RegistrationScreen> rule  = new  ActivityTestRule<>(RegistrationScreen.class);


    @Test
    public void passwordValidationTrue() {
        final RegistrationScreen testRegScren = rule.getActivity();
        testRegScren.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                testRegScren.validator.validatePassword("Hello2017","Hello2017");
            }
        });

        assertThat(testRegScren.validPass, is(true));
    }

    @Test
    public void passwordValidationFalse() {
        final RegistrationScreen testRegScren = rule.getActivity();
        testRegScren.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                testRegScren.validator.validatePassword("Hello2017","Hello2016");
            }
        });

        assertThat(testRegScren.validPass, is(false));
    }

    @Test
    public void emailValidationTrue() {
        final RegistrationScreen testRegScren = rule.getActivity();
        testRegScren.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                testRegScren.validator.validateEmail("abc@internet.com");
            }
        });
        assertThat(testRegScren.validEmail, is(true));
    }

    @Test
    public void emailValidationFalse() {
        final RegistrationScreen testRegScren = rule.getActivity();
        testRegScren.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                testRegScren.validator.validateEmail("abc@internetcom");
            }
        });
        assertThat(testRegScren.validEmail, is(false));
    }
}
