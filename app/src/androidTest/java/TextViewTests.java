import android.support.test.rule.ActivityTestRule;
import android.view.View;
import android.widget.ProgressBar;

import org.junit.Rule;
import org.junit.Test;

import projects.exorno.greenflagapp.customwidgets.CustomTextView;
import projects.exorno.greenflagapp.ui.RegistrationScreen;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;


/**
 * Created by marios on 2017/11/01.
 */

public class TextViewTests {

    @Rule
    public ActivityTestRule<RegistrationScreen> rule  = new  ActivityTestRule<>(RegistrationScreen.class);


    @Test
    public void emailErrTextTypeCheck() {
        RegistrationScreen testRegScren = rule.getActivity();
        View textview = testRegScren.emailErrText;
        assertThat(textview, instanceOf(CustomTextView.class));
    }

    @Test
    public void passErrTextTypeCheck() {
        RegistrationScreen testRegScren = rule.getActivity();
        View textview = testRegScren.passErrText;
        assertThat(textview, instanceOf(CustomTextView.class));
    }

}
